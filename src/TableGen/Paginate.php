<?php

namespace TableGen;

class Paginate
{
    private $db_host;
    private $db_user;
    private $db_pass;
    private $db_name;
    private $db;

    public function __construct($params = array())
    {
        $this->db_host = (isset($params['db_host']) ? $params['db_host'] : 'localhost');
        $this->db_user = (isset($params['db_user']) ? $params['db_user'] : 'root');
        $this->db_pass = (isset($params['db_pass']) ? $params['db_pass'] : '');
        $this->db_name= (isset($params['db_name']) ? $params['db_name'] : 'testdb');
        $this->db = $this->connect();
    }

    public function connect()
    {
        try {
            $DB_con = new \PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $this->db_user, $this->db_pass);
            $DB_con->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $DB_con;
        } catch (\PDOException $exception) {
            echo $exception->getMessage();
            return null;
        }
    }

    public function dataview($query)
    {
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        ?>
        <table class="table  myTableClass"  id="data">
            <thead>
            <th>ID</th>
            <th>Product Code</th>
            <th>Product Name</th>
            <th>Product Line</th>
            <th>Text Description</th>
            <th>Product Description</th>
            </thead>
        <?php
        if ($stmt->rowCount()>0) {
            while ($product=$stmt->fetch(\PDO::FETCH_ASSOC)) {
                ?>
                <tr>
                    <td> <?php echo $product['id'] ?> </td>
                    <td> <?php echo $product['productCode'] ?> </td>
                    <td> <?php echo $product['productName'] ?> </td>
                    <td> <?php echo $product['productLine'] ?> </td>
                    <td> <?php echo $product['textDescription'] ?> </td>
                    <td> <?php echo $product['productDescription'] ?> </td>
                </tr>
                <?php
            } ?>

            <?php
        } else { ?>
            <tr>
                <td>Nothing here...</td>
            </tr>
            <?php
        }
    }

    public function paging($query, $per_page)
    {
        $starting_position=0;
        if (isset($_GET["page_no"])) {
            $starting_position=($_GET["page_no"]-1)*$per_page;
        }
        $query2=$query." limit $starting_position,$per_page";
        return $query2;
    }

    public function paginglink($query, $per_page)
    {

        $self = $_SERVER['PHP_SELF'];

        $stmt = $this->db->prepare($query);
        $stmt->execute();

        $total_no_of_records = $stmt->rowCount();


        if ($total_no_of_records > 0) {
            ?><tr class="text-center"><td colspan="6"><ul class="pagination">
                    <?php
                    $total_no_of_pages=ceil($total_no_of_records/$per_page);
                    $current_page=1;
            if (isset($_GET["page_no"])) {
                $current_page=$_GET["page_no"];
            }
            if ($current_page!=1) {
                $previous =$current_page-1;
                echo "<li><a href='".$self."?page_no=".$previous."'>Previous</a></li>";
            } else {
                echo "<li class='disabled'><a href=''>Previous</a></li>";
            }
            if ($current_page!=$total_no_of_pages) {
                $next=$current_page+1;
                echo "<li><a href='".$self."?page_no=".$next."'>Next</a></li>";
            } else {
                echo "<li class='disabled'><a href=''>Next</a></li>";
            }
            ?></ul></td></tr></table><?php
        }
    }
}

