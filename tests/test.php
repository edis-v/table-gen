<?php

require_once __DIR__ . '/../vendor/autoload.php';

use TableGen\Paginate;

?>
<link href="../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
<script src="../vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
<?php
$paginate = new Paginate();

$query = "SELECT * FROM products";
$per_page=2;
$query2 = $paginate->paging($query, $per_page);
$paginate->dataview($query2);
$paginate->paginglink($query, $per_page);
